package fr.snyker.utils;

import org.bukkit.entity.Player;

public enum Utils {
	
	TITLE_PLUGIN("§f[§bSANCTIONS+§f]§r "),
	SEPARATOR("§8=$=$=$=$=$=$=$=$=$=$=$=$=$=$=$=$=$=$=$=$=$=$=$=$="),
	SPACE("    "),
	PLAYER_NOT_FOUND("§cLe joueur n'existe pas ou n'est pas connecté.");
	
	private String msg;
	
	Utils(String msg){
		this.msg = msg;
	}
	
	public String getMsg(){
		return msg;
	}
	
	public static void getUserStats(Player player, Player target){
		player.sendMessage(Utils.SEPARATOR.getMsg());
		player.sendMessage("§bVoici les renseignements pour le joueur : §f§n" + target.getName());
		player.sendMessage(Utils.SPACE.getMsg() + "§cIp : §6" + ManagerConfiguration.getIp(target.getUniqueId().toString()));
		player.sendMessage(Utils.SPACE.getMsg() + "§cAvertissement : §6" + ManagerConfiguration.getAvertissement(target.getUniqueId().toString()));
		player.sendMessage(Utils.SPACE.getMsg() + "§cMute : §6" + ManagerConfiguration.isMute(target.getUniqueId().toString()));
		player.sendMessage(Utils.SPACE.getMsg() + "§cBan : §6" + ManagerConfiguration.isBan(target.getUniqueId().toString()));
		player.sendMessage(Utils.SEPARATOR.getMsg());
	}

}
