package fr.snyker.utils;

import java.io.File;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import fr.snyker.Sanctions;

public class ManagerConfiguration {

	static File file;
	static FileConfiguration fileConfig;
	
	public ManagerConfiguration(){
		if(!Sanctions.getPlugin().getDataFolder().exists()){
			Sanctions.getPlugin().getDataFolder().mkdir();
		}
		
		file = new File(Sanctions.getPlugin().getDataFolder(), "user.yml");
		fileConfig = YamlConfiguration.loadConfiguration(file); 
		
		if(!file.exists()){
			try{
				file.createNewFile();
			} catch(IOException e){
				e.printStackTrace();
			}
		}
		
	}

	public static void newFile(String pseudo, String uuid, String ip) {

		if(!fileConfig.equals(pseudo)){
			fileConfig.set("players."+uuid+".player", pseudo);
			fileConfig.set("players."+uuid+".ip", ip);
			fileConfig.set("players."+uuid+".avertissement", 0);
			fileConfig.set("players."+uuid+".mute", false);
			fileConfig.set("players."+uuid+".ban", false);
			
			saveConfig();
		}
		
	}
	
	public static FileConfiguration getConfig(){
		return fileConfig;
	}
	
	public static void saveConfig(){
		try{
			fileConfig.save(file);
		} catch(IOException e){
			Bukkit.getServer().getLogger().severe("Impossible de sauvegarder user-data.yml");
		}
	}
		
	public static void reloadConfig(){
		saveConfig();
		fileConfig = YamlConfiguration.loadConfiguration(file);
	}
	
	public static String getIp(String uuid){
		return getConfig().getString("players."+uuid+".ip");
	}
	
	public static int getAvertissement(String uuid){
		return getConfig().getInt("players."+uuid+".avertissement");
	}
	
	public static boolean isMute(String uuid){
		return getConfig().getBoolean("players."+uuid+".mute");
	}
	
	public static boolean isBan(String uuid){
		return getConfig().getBoolean("players."+uuid+".ban");
	}
	
	public static void setIp(String uuid, String value){
		getConfig().set("players."+uuid+".ip", value);
	}
	
	public static void setAvertissement(String uuid, int value){
		getConfig().set("players."+uuid+".avertissement", value);
	}
	
	public static void setMute(String uuid, boolean value){
		getConfig().set("players."+uuid+".mute", value);
	}
	
	public static void setBan(String uuid, boolean value){
		getConfig().set("players."+uuid+".ban", value);
	}
	
}
