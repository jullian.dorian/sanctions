package fr.snyker.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.snyker.utils.ManagerConfiguration;
import fr.snyker.utils.Utils;

public class SanctionsCommands implements CommandExecutor{

	@SuppressWarnings("all")
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if(!(sender instanceof Player)){
			Bukkit.getConsoleSender().sendMessage("Seulement les joueurs peuvent utiliser la commande.");
			return false;
		}
		
		Player p = (Player) sender;
		
		if(cmd.getName().equalsIgnoreCase("sanctions")){
			
			if(args.length != 0){
				
					if(args[0].equalsIgnoreCase("see")
							|| args[0].startsWith("s")){
						
						if(args.length == 2){

							Player target = Bukkit.getPlayerExact(args[1]);
							
							if(target != null){
								Utils.getUserStats(p, target);
							} else {
								p.sendMessage(Utils.TITLE_PLUGIN.getMsg()+"§cLe joueur n'existe pas.");
							}
						}  else {
							p.sendMessage(Utils.TITLE_PLUGIN.getMsg()+"§cVous devez entrez un nom de joueur.");
						}
					} else 
					
					if(args[0].equalsIgnoreCase("avertissement")
							|| args[0].startsWith("a")
							|| args[0].equalsIgnoreCase("avert")){
						if(args.length == 2){

							Player target = Bukkit.getPlayerExact(args[1]);
							
							if(target != null){
								
							} else {
								p.sendMessage(Utils.TITLE_PLUGIN.getMsg()+"§cLe joueur n'existe pas.");
							}
							
						} else {
							p.sendMessage(Utils.TITLE_PLUGIN.getMsg()+"§cVous devez entrez un nom de joueur.");
						}
						
					} else
						
					if(args[0].equalsIgnoreCase("mute")
							|| args[0].startsWith("m")){
						if(args.length == 2){

							Player target = Bukkit.getPlayerExact(args[1]);
							
							if(target != null){		
							} else {
								p.sendMessage(Utils.TITLE_PLUGIN.getMsg()+"§cLe joueur n'existe pas.");
							}
							
						}
					} else
						
					if(args[0].equalsIgnoreCase("ban")
							|| args[0].startsWith("b")){
						if(args.length >= 2){

							Player target = Bukkit.getPlayerExact(args[1]);
							
							if(target != null){
								
								p.sendMessage(Utils.TITLE_PLUGIN.getMsg() + "§f"+target.getName()+"§aà reçu un avertissement !");
								target.sendMessage(Utils.TITLE_PLUGIN.getMsg() + "§f"+p.getName()+"§cvous à donner un avertissement.");
								
								ManagerConfiguration.setAvertissement(target.getUniqueId().toString(), ManagerConfiguration.getAvertissement(target.getUniqueId().toString())+1);
								
							}
							
						}  else {
							p.sendMessage(Utils.TITLE_PLUGIN.getMsg()+"§cVous devez entrez un nom de joueur.");
						}
					}
					
			} else {
				p.sendMessage(Utils.TITLE_PLUGIN.getMsg() + "§cVous devez précisez des arguments.");
				return false;
			}
			
		}
		
		return false;
	}

}
