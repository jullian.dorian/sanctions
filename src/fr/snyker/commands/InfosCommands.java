package fr.snyker.commands;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.snyker.utils.Utils;

public class InfosCommands implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if(!(sender instanceof Player)){
			Bukkit.getConsoleSender().sendMessage("Seulement les joueurs peuvent lancer la commande.");
			return false;
		}
		
		Player p = (Player) sender;
		
		if(cmd.getName().equalsIgnoreCase("view")){
			if(args.length == 0){ 
				return false;
			}
			
			if(args.length == 1){
				
				//DETECTION DE LIP DUN JOUEUR
				Player target = Bukkit.getPlayerExact(args[0]);
				
				//DETECTION DES JOUEURS VIA LIP				
				if(target != null){
					String ip = target.getAddress().getHostName();
					p.sendMessage(Utils.TITLE_PLUGIN.getMsg()+"§f"+target.getName()+"§e : §6"+target.getAddress().getAddress().getHostAddress());
					
					ArrayList<String> listName2 = new ArrayList<>();
					
					//ON ajoute tous les joueurs ayant la même ip dans la liste
					for(Player pls : Bukkit.getOnlinePlayers()){
						if(ip.equals(pls.getAddress().getHostName())){
							listName2.add(pls.getName());
						}
					}
					//On envoie la commande
					p.sendMessage(Utils.SEPARATOR.getMsg());
					p.sendMessage(Utils.TITLE_PLUGIN.getMsg()+"§eIp :  §6"+target.getAddress().getAddress().getHostAddress());
					p.sendMessage(Utils.SEPARATOR.getMsg());
					p.sendMessage(Utils.TITLE_PLUGIN.getMsg()+"§aListes des joueurs suivant l'ip : §b"+listName2.toString());
					
					//On supprime la liste
					listName2.removeAll(listName2);
					
				} else {
					p.sendMessage(Utils.TITLE_PLUGIN.getMsg()+Utils.PLAYER_NOT_FOUND.getMsg());
					return false;
				}
			}
		}
		
		return false;
	}

}

/* if(ip != null && ip.length() <= 12){
					
		String[] blackWord = {"a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"
				,"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"};
		
		if(ip.equals(blackWord)) return false;
		
		ArrayList<String> listName2 = new ArrayList<>();
		
		//ON ajoute tous les joueurs ayant la même ip dans la liste
		for(Player pls : Bukkit.getOnlinePlayers()){
			if(ip.equals(pls.getAddress().getHostName())){
				listName2.add(pls.getName());
			}
		}
		//On envoie la commande
		p.sendMessage("§aLes joueurs ayant la même ip sont : §b"+listName2.toString());
		
		//On supprime la liste
		listName2.removeAll(listName2);
	}
*/
