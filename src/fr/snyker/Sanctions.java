package fr.snyker;

import org.bukkit.plugin.java.JavaPlugin;

import fr.snyker.commands.InfosCommands;
import fr.snyker.commands.SanctionsCommands;
import fr.snyker.listener.ManagerListener;
import fr.snyker.utils.ManagerConfiguration;

public class Sanctions extends JavaPlugin{

	
	private static Sanctions instance;
	
	public Sanctions() {}
	
	
	@Override
	public void onEnable() {
		instance = this;
		new ManagerConfiguration();
		
		new ManagerListener(this);
		
		getCommand("sanctions").setExecutor(new SanctionsCommands());
		getCommand("view").setExecutor(new InfosCommands());
		
		getConfig().options().copyDefaults(true);
		saveConfig();
		
		super.onEnable();
	}


	@Override
	public void onDisable() {
		saveConfig();
		super.onDisable();
	}
	
	public static Sanctions getPlugin(){
		return instance;
	}
	
}
