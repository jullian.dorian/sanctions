package fr.snyker.listener;

import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;

import fr.snyker.listener.events.JoinServer;

public class ManagerListener{

	public ManagerListener(Plugin pl){
		PluginManager pm = Bukkit.getPluginManager();
		
		pm.registerEvents(new JoinServer(), pl);
	}
	
}
